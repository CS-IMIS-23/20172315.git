import junit.framework.TestCase;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ComplexTest{
 Complex a= new Complex(1,2);
 Complex b= new Complex(1,-4);
 Complex c= new Complex(-1,1);
 Complex d= new Complex(0,1);
 Complex e= new Complex(1,0);
 @Test
    public void getRealpart() throws Exception{
     assertEquals("1.0",""+a.getRealPart());
     assertEquals("-4.0",""+b.getImagePart());
     assertEquals("-1.0",""+c.getRealPart());
     assertEquals("0.0",""+d.getRealPart());
     assertEquals("0.0",""+e.getImagePart());
 }
    @Test
    public void Complexjia() throws Exception {
     a.Complexjia(b);
     assertEquals(true, a.equal(2, -2));
 }
 @Test
    public void Complexjian() throws Exception{
     a.Complexjian(b);
     assertEquals(true,a.equal(0,6));
 }
 @Test
    public void  Complexcheng() throws Exception{
     a= a.Complexcheng(b);
     assertEquals(true,a.equal(9.0,-2.0));
 }
 @Test
    public void Complexchu() throws Exception{
     a= a.Complexchu(b);
     assertEquals(true,a.equal(0.5294117647058824,2.124567474048443));
 }
}

