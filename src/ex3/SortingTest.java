package cn.edu.besti.cs1723.HZT2315;
import junit.framework.TestCase;

public class SortingTest extends TestCase {
    Integer[] a = {2, 3, 1, 5};
    Integer[] b = {1, 2, 3, 5};
    Integer[] c = {99999999,2,3,1,5};
    Integer[] d = {1,2,3,5,99999999};
    Integer[] e= {5,3,2,1};
    public void testSelectionSort() {
        Sorting.selectionSort(a);
        for (int i=0;i<a.length;i++)
        assertEquals(b[i],a[i]);
    }
    public void testSelectionSort1() {
        Sorting.selectionSort(c);
        for (int i=0;i<a.length;i++)
            assertEquals(d[i],c[i]);
    }
    public void testSelectionSort2() {
        Sorting.selectionSort(a);
        boolean result1 = true;
        for (int i =0 ;i<a.length;i++)
        {
            if (a[i]!=c[i])
            {
                result1 = false;
                break;
            }
        }
        assertEquals(false,result1);

    }
    public void testSelectionSort3() {
        Sorting.selectionSort(a);
        for (int i = 0; i < a.length; i++)
            assertEquals(e[e.length - 1 - i], a[i]);
    }
}