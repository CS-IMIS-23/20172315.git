public class test1
{
    public static void main(String[] args) throws NonComparableElementException {
        int[] a = {1, 3, 5, 7, 9, 33, 12, 34};
        int[] b = {1,3,5};
        Integer[] c = {2, 3, 1, 5};
        System.out.println("数组a为：");
        for(int i = 0; i<a.length;i++){
            System.out.print(a[i]+" ");
        }
        System.out.println();
        System.out.println("数组c为：");
        for(int i = 0; i<c.length;i++){
            System.out.print(c[i]+" ");
        }
        System.out.println();
        System.out.println("二叉树查找" + Searching.BinaryTreeSearch(c, 5));
        System.out.println("二分查找" + Searching.binarySearch1(a, 5));
        System.out.println("顺序表查找" + Searching.sequentialSearch2(a, 5));
        System.out.println("插值查找" + Searching.interpolationSearch(a, 5));
        System.out.println("斐波那契查找" + Searching.FibonacciSearch(a,2,3));
        System.out.println("分块查找" + Searching.blockSearch(a,b,1,4));
        System.out.println("哈希查找" + Searching.Hashsearch(a,2,3));
    }
}



