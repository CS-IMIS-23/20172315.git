import java.text.NumberFormat;

public class DVDCollection {
    private DVDNode list;

    //Sets up an intially empty list of DVDs.
    public DVDCollection() {
        list = null;
    }

    //Creates a new DVDNode object and adds it to the end of the linked list.
    public void add(DVD D) {
        DVDNode node = new DVDNode(D);
        DVDNode current;

        if(list == null)
            list = node;
        else
        {   current = list;
            while(current.next!=null)
                current = current.next;
            current.next = node;
        }
    }
    public String toString()
    {
        String result = "";
        DVDNode current = list;
        while(current != null)
        {
            result += current.dvd + "\n";
            current = current.next;
        }
        return result;
    }

    private class DVDNode {
        public DVD dvd;
        public DVDNode next;

        public DVDNode(DVD D) {
            dvd = D;
            next = null;
        }
    }
}
