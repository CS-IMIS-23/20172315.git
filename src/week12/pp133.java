import java.util.Scanner;

public class pp133 {

    public static void main(String[] args) {
        Node no = addNode();
        P(no);
        P(sortNode(no));
    }

    // 打印单链表
    static void P(Node a) {
        a = a.next;
        while (a != null) {
            System.out.print(a.data + " ");
            a = a.next;
        }
        System.out.println();
    }

    // 添加元素
    static Node addNode() {

        Node head = new Node(0);
        Node t = null;
        t = head;
        System.out.println("输入：");
        System.out.println("输入0即可停止输入");
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        do {
            Node tail = new Node();
            tail.data = a;
            tail.next = null;
            head.next = tail;
            head = tail;
        } while (sc.hasNext() && (a = sc.nextInt()) != 0);
        sc.close();
        return t;
    }

    // 直接选择排序单链表
    static Node sortNode(Node head) {
        Node t = null;
        t = head;
        int min = 0;
        int temp = 0;
        Node p, q, m;
        p = q = m = head;
        while (p.next != null) {
            m = q = p.next;
            min = q.data;
            while (q != null) {
                if (q.data < min) {
                    m = q;
                    min = q.data;
                }
                q = q.next;
            }
            if (p.next != m) {
                temp = p.next.data;
                p.next.data = m.data;
                m.data = temp;
            }
            p = p.next;
        }
        return t;
    }

}

class Node {
    int data;
    Node next;

    public Node(int data) {
        this.data = data;
        next = null;
    }

    public Node() {
    }
}