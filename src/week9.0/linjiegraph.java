import java.util.*;

public class linjiegraph<T> implements GraphADT<T> {

    protected final int DEFAULT_CAPACITY = 5;
    protected int numVertices;    // number of vertices in the graph
    protected T[] vertices;    // values of vertices
    protected int modCount;
    protected ArrayList<Nodea> node;
    public linjiegraph()
    {
        modCount =0;
        numVertices = 0;
        node = new ArrayList<Nodea>();
    }

    @Override//添加顶点
    public void addVertex(T vertex) {
        Nodea a = new Nodea(vertex);
        node.add(a);
        numVertices++;
        modCount++;
    }
    @Override
    public void removeVertex(T vertex) {
        Nodea vertexNode = new Nodea(vertex);
        node.remove(vertexNode);
        numVertices--;
        modCount++;
    }
    public void addEdge(T vertex1, T vertex2) {

        int i = 0;
        while (node.get(i).getElement() != vertex1) {
            i++;
        }
        Nodea temp = node.get(i);
        while (temp.getNext() != null) {//找到temp链表的末端
            temp = temp.getNext();
        }
        temp.setNext(new Nodea(vertex1));

        int j = 0;
        while (node.get(j).getElement() != vertex2) {
            j++;
        }
        Nodea temp1 = node.get(j);
        while (temp1.getNext() != null) {
            temp1 = temp1.getNext();
        }
        temp1.setNext(new Nodea(vertex1));
    }
    public void removeEdge(T vertex1, T vertex2) {
        int i = 0;
        while (node.get(i).getElement() != vertex1) {
            i++;
        }
        Nodea temp = node.get(i);
        while (temp.getNext().getElement() != vertex2) {
            temp = temp.getNext();
        }
        if (temp.getNext().getNext() == null) {
            temp.setNext(null);
        } else {
            temp.getNext().setNext(temp.getNext().getNext());
        }
    }
    public Iterator iteratorBFS(T startVertex) {
        return iteratorBFS(getIndex(startVertex));

    }
    public Iterator<T> iteratorBFS(int startIndex)
    {
        Integer x;
        QueueADT<Integer> traversalQueue = new LinkedQueue<Integer>();
        UnorderedListADT<T> resultList = new ArrayUnorderedList<T>();

        if (!indexIsValid(startIndex))
            return resultList.iterator();

        boolean[] visited = new boolean[numVertices];
        for (int i = 0; i < numVertices; i++)
            visited[i] = false;

        traversalQueue.enqueue(startIndex);
        visited[startIndex] = true;

        while (!traversalQueue.isEmpty())
        {
            x = traversalQueue.dequeue();
            resultList.addToRear((T)node.get(x).getElement());

            //Find all vertices adjacent to x that have not been visited
            //     and queue them up

            for (int i = 0; i < numVertices; i++)
            {
                if (isEdge(x,i) && !visited[i])
                {
                    traversalQueue.enqueue(i);
                    visited[i] = true;
                }

            }
        }
        return new GraphIterator(resultList.iterator());
    }

    protected class GraphIterator implements Iterator<T> {
        private int expectedModCount;
        private Iterator<T> iter;


        public Iterator<T> iteratorDFS(int startIndex)
        {
            Integer x;
            boolean found;
            T[] vertices = (T[]) (new Object[numVertices]);
            StackADT<Integer> traversalStack = new LinkedStack<Integer>();
            UnorderedListADT<T> resultList = new ArrayUnorderedList<T>();
            boolean[] visited = new boolean[numVertices];

            if (!indexIsValid(startIndex))
                return resultList.iterator();

            for (int i = 0; i < numVertices; i++)
                visited[i] = false;

            traversalStack.push(startIndex);
            resultList.addToRear((T)node.get(startIndex).getElement());
            visited[startIndex] = true;

            while (!traversalStack.isEmpty())
            {
                x = traversalStack.peek();
                found = false;

                //Find a vertex adjacent to x that has not been visited
                //     and push it on the stack
                for (int i = 0; (i < numVertices) && !found; i++)
                {
                    if (isEdge(x,i) && !visited[i])
                    {
                        traversalStack.push(i);
                        resultList.addToRear((T)node.get(i).getElement());
                        visited[i] = true;
                        found = true;
                    }
                }
                if (!found && !traversalStack.isEmpty())
                    traversalStack.pop();
            }
            return new GraphIterator(resultList.iterator());
        }

        public boolean isEdge(int i, int j){
            if(i==j)
                return false;
            Nodea vertexNode1 = node.get(i);
            Nodea vertexNode2 = node.get(j);
            while(vertexNode1!=null) {
                if (vertexNode1.getElement() == vertexNode2.getElement())
                    return true;
                vertexNode1 =vertexNode1.getNext();
            }
            return false;
        }

        protected boolean indexIsValid(int index)
        {
            if (index<numVertices)
                return true;
            else
                return false;
        }


        public Iterator iteratorShortestPath(T startVertex, T targetVertex) {
            return iteratorShortestPath(getIndex(startVertex), getIndex(targetVertex));
        }

        public Iterator<T> iteratorShortestPath(int startIndex, int targetIndex)
        {
            UnorderedListADT<T> resultList = new ArrayUnorderedList<T>();
            if (!indexIsValid(startIndex) || !indexIsValid(targetIndex))
                return resultList.iterator();

            Iterator<Integer> it = iteratorShortestPathIndices(startIndex,
                    targetIndex);
            while (it.hasNext())
                resultList.addToRear((T)node.get(((Integer)it.next())).getElement());
            return new GraphIterator(resultList.iterator());
        }

        protected Iterator<Integer> iteratorShortestPathIndices
                (int startIndex, int targetIndex)
        {
            int index = startIndex;
            int[] pathLength = new int[numVertices];
            int[] predecessor = new int[numVertices];
            QueueADT<Integer> traversalQueue = new LinkedQueue<Integer>();
            UnorderedListADT<Integer> resultList =
                    new ArrayUnorderedList<Integer>();

            if (!indexIsValid(startIndex) || !indexIsValid(targetIndex) ||
                    (startIndex == targetIndex))
                return resultList.iterator();

            boolean[] visited = new boolean[numVertices];
            for (int i = 0; i < numVertices; i++)
                visited[i] = false;

            traversalQueue.enqueue(Integer.valueOf(startIndex));
            visited[startIndex] = true;
            pathLength[startIndex] = 0;
            predecessor[startIndex] = -1;

            while (!traversalQueue.isEmpty() && (index != targetIndex))
            {
                index = (traversalQueue.dequeue()).intValue();

                //Update the pathLength for each unvisited vertex adjacent
                //     to the vertex at the current index.
                for (int i = 0; i < numVertices; i++)
                {
                    if (isEdge(index,i) && !visited[i])
                    {
                        pathLength[i] = pathLength[index] + 1;
                        predecessor[i] = index;
                        traversalQueue.enqueue(Integer.valueOf(i));
                        visited[i] = true;
                    }
                }
            }
            if (index != targetIndex)  // no path must have been found
                return resultList.iterator();

            StackADT<Integer> stack = new LinkedStack<Integer>();
            index = targetIndex;
            stack.push(Integer.valueOf(index));
            do
            {
                index = predecessor[index];
                stack.push(Integer.valueOf(index));
            } while (index != startIndex);

            while (!stack.isEmpty())
                resultList.addToRear(((Integer)stack.pop()));

            return new GraphIndexIterator(resultList.iterator());
        }

        public boolean isEmpty() {
            if (size() != 0)
                return true;
            else
                return false;
        }

        public boolean isConnected() {
            boolean result = true;
            for(int i=0;i<numVertices;i++){
                int temp=0;
                temp=getSizeOfIterator(iteratorBFS(i));
                if(temp!=numVertices)
                {
                    result = false;
                    break;
                }
            }
            return result;
        }
        private int getSizeOfIterator(Iterator iterator) {
            int size = 0;
            while(iterator.hasNext()){
                size++;
                iterator.next();
            }
            return size;
        }





            public GraphIterator(Iterator<T> iter)
            {
                this.iter = iter;
                expectedModCount = modCount;
            }



            public boolean hasNext() throws ConcurrentModificationException
            {
                if (!(modCount == expectedModCount))
                    throw new ConcurrentModificationException();

                return (iter.hasNext());
            }


            public T next() throws NoSuchElementException
            {
                if (hasNext())
                    return (iter.next());
                else
                    throw new NoSuchElementException();
            }


            public void remove()
            {
                throw new UnsupportedOperationException();
            }
        }


        public class GraphIndexIterator implements Iterator<Integer>
        {
            private int expectedModCount;
            private Iterator<Integer> iter;


            public GraphIndexIterator(Iterator<Integer> iter)
            {
                this.iter = iter;
                expectedModCount = modCount;
            }


            public boolean hasNext() throws ConcurrentModificationException
            {
                if (!(modCount == expectedModCount))
                    throw new ConcurrentModificationException();

                return (iter.hasNext());
            }


            public Integer next() throws NoSuchElementException
            {
                if (hasNext())
                    return (iter.next());
                else
                    throw new NoSuchElementException();
            }


            public void remove()
            {
                throw new UnsupportedOperationException();
            }
        }
    }



