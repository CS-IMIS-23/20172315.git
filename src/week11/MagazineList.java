public class MagazineList
{
    private MagazineNode list;

    public MagazineList()
    {
        list = null;
    }


    public void add(Magazine mag)
    {
        MagazineNode node = new MagazineNode(mag);
        MagazineNode current;

        if (list == null)
            list = node;
        else
        {
            current = list;
            while (current.next != null)
                current = current.next;
            current.next = node;
        }
    }
    public  void insert(int index, Magazine mag){
        MagazineNode a = list;
        MagazineNode newMagazine = new MagazineNode(mag);
        if (index == 0) {
            newMagazine = list;
            newMagazine.next = a;
        }
        else {
           int b = 0;
            while (b < index - 1) {
                a = a.next;
                b++;
            }
        }
        newMagazine.next = a.next;
        a.next = newMagazine;

    }//在index的位置插入新节点newMagazine
    public void delete(Magazine mag){
        MagazineNode delNode = list;
        while(delNode.next.magazine != mag){
            delNode = delNode.next;
        }
        delNode.next = delNode.next.next;
    }


//删除节点delNode
    public String toString()
    {
        String result = "";

        MagazineNode current = list;

        while (current != null)
        {
            result += current.magazine + "\n";
            current = current.next;
        }

        return result;
    }


    private class MagazineNode
    {
        public Magazine magazine;
        public MagazineNode next;

        public MagazineNode(Magazine mag)
        {
            magazine = mag;
            next = null;

        }
    }
}
