package cn.edu.besti.cs1723;

public class Node {
    public String code = "";
public String data = "";
public int count;
public Node lChild;
public Node rChild;
public Node() {

} public Node(String data, int count) {
    this.data = data; this.count = count;
} public Node
            (int count, Node lChild, Node rChild)
    {
    this.count = count;
    this.lChild = lChild;
    this.rChild = rChild;
    } public Node(
            String data,
            int count,
            Node lChild,
            Node rChild) {
    this.data = data;
    this.count = count;
    this.lChild = lChild;
    this.rChild = rChild;
}
}
