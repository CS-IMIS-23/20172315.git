import java.util.Arrays;
import java.util.EmptyStackException;
import java.util.StringTokenizer;

public class ArrayStack<T> implements StackADT<T> {
    private final int DEFAULT_CAPACITY =100;

    private T[] stack;
    private int nHuZhitao;
    public ArrayStack()
    {
        nHuZhitao = 0;
        stack = (T[]) (new Object[DEFAULT_CAPACITY]);
    }

    public ArrayStack(int initialCapacity)
    {
        nHuZhitao = 0;
        stack = (T[])(new Object[initialCapacity]);
    }
    public void push(T element){
        if(size() == stack.length)
            expandCapacity();
        stack[nHuZhitao] = element;
        nHuZhitao++;
    }
    private void expandCapacity()

    {
        stack = Arrays.copyOf(stack,stack.length * 2);
    }
    public T pop() throws EmptyCollectionException{
        if(isEmpty()) {
            throw new EmptyCollectionException("Stack");
        }
        nHuZhitao--;
        T result = stack[nHuZhitao];
        stack[nHuZhitao] = null;

        return result;
    }

    @Override
    public T peek() throws EmptyCollectionException
    {
        if(isEmpty()) {
            throw new EmptyCollectionException("Stack");
        }
        return stack[nHuZhitao-1];
    }
    @Override
    public boolean isEmpty()
    {
        // To be completed as a Programming Project
        if(size() == 0) {
            return true;
        } else {
            return false;
        }
    }
    @Override
    public int size()
    {
        return nHuZhitao;
    }
    public void insert(T element, int n){
        T[] a = (T[])(new Object[size() + 1]);
        for(int i =0; i < n;i ++)
            a[i] = stack[i];
        a[n] = element;
        for(int i = n + 1;i < size() + 1; i ++ ) {
            a[i] = stack[i - 1];
        }
        stack = a;
        nHuZhitao ++;
    }

    public void delete(int n){
        T[] a = (T[])(new Object[size() - 1 ]);
        for(int i = 0;i < n; i++)
            a[i] = stack[i];
        for(int i = n;i < size() - 1; i++)
            a[i] = stack[i+1];
        stack = a;
        nHuZhitao--;
    }
    public static  String sort(String s){
        String result="";
        StringTokenizer stringTokenizer = new StringTokenizer(s);
        int [] score = new int[stringTokenizer.countTokens()];
        for (int i =0;stringTokenizer.hasMoreTokens();i++){
            score[i]=Integer.parseInt(stringTokenizer.nextToken());
        }
        for(int i =0;i < score.length - 1;i++){//使用冒泡排序
            for(int j = 0;j <  score.length - 1-i;j++){ // j开始等于0，
                if(score[j] < score[j+1]) {
                    int temp = score[j];
                    score[j] = score[j+1];
                    score[j+1] = temp;
                }
            }
        }
        for (int i =0 ;i<score.length;i++){
            result+= score[i]+ " ";
        }
        return result ;
    }



    @Override
    public String toString()
    {
        String A ="";
        for(int i = 0 ; i < nHuZhitao; i++)
        {
            A +=  stack[i]+" ";
        }
        return A;
    }
}


