import java.io.*;

/**
 * BackPainAnaylyzer demonstrates the use of a binary decision tree to 
 * diagnose back pain.
 */
public class BackPainAnalyzer
{
    /**
     *  Asks questions of the user to diagnose a medical problem.
     */
    public static void main (String[] args) throws FileNotFoundException
    {
        System.out.println ("So, you're having back pain.");

        DecisionTree expert = new DecisionTree("input.txt");
        expert.evaluate();
        System.out.println("树中叶节点个数是" +expert.jisuan(expert.getTree().getRootNode()));

        LinkedBinaryTree linkedBinaryTree=expert.getTree();
        System.out.println("树的深度是" +linkedBinaryTree.getHeight());
    }
}
