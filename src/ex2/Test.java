import java.util.Iterator;


public class Test {
    public static void main(String[] args) {
        LinkedBinaryTree tree3 = new LinkedBinaryTree (2);
        LinkedBinaryTree  tree4 = new LinkedBinaryTree (0);
        LinkedBinaryTree  tree6 = new LinkedBinaryTree (1);
        LinkedBinaryTree  tree8 = new LinkedBinaryTree (7);
        LinkedBinaryTree  tree9 = new LinkedBinaryTree (2);
        LinkedBinaryTree  tree7 = new LinkedBinaryTree (3,tree8,tree9);
        LinkedBinaryTree  tree5 = new LinkedBinaryTree (1,tree6,tree7);
        LinkedBinaryTree tree2 = new LinkedBinaryTree(5,tree3,tree4);
        LinkedBinaryTree  tree1 = new LinkedBinaryTree (1,tree2,tree5);

        System.out.println(tree1);
        System.out.println("tree2的右子树"+ tree2.getRight());

        System.out.println( "有1吗？："+ tree1.contains(1));
        System.out.print("前序遍历：");
        Iterator l1=tree1.iteratorPreOrder();
        while (l1.hasNext())
            System.out.println(l1.next()+" ");;


        System.out.print("后序遍历：");
        Iterator l2=tree1.iteratorPostOrder();
        while (l2.hasNext())
            System.out.println(l2.next()+" ");

    }
}

